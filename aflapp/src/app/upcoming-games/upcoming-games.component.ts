import { Component, OnInit } from '@angular/core';
import { DataserviceService } from '../dataservice.service';
import { Game } from '../game';
@Component({
  selector: 'app-upcoming-games',
  templateUrl: './upcoming-games.component.html',
  styleUrls: ['./upcoming-games.component.css']
})
export class UpcomingGamesComponent implements OnInit {
  upcomingGames: Game[];
  constructor(private dataService: DataserviceService) { }

  ngOnInit(): void {
    this.getUpcomingGames();
  }
  // not using element.complete because the brief specifies that the we are at round 20.
  //Method collects upcoming games for next round into an array for display in html.
  getUpcomingGames(): void {
    this.dataService.getGames().subscribe(temp => {
      var tempArr = [];

      temp.forEach(element => {
        if (element.year == 2019 && element.round == 20) {
          tempArr.push(element);
        };
      });
      this.upcomingGames = tempArr;
    })
  }
}

