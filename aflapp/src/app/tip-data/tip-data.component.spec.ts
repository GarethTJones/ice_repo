import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipDataComponent } from './tip-data.component';

describe('TipDataComponent', () => {
  let component: TipDataComponent;
  let fixture: ComponentFixture<TipDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
