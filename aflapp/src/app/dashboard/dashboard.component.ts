import { Component, OnInit } from '@angular/core';
import { DataserviceService } from '../dataservice.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private dataService: DataserviceService) { }

  venues: string[];
  venueCounter = 0;
  selectedVenue = null;

  ngOnInit(): void {
    this.getVenues();
  }

  onSelect(venue: string): void {
    this.selectedVenue = venue;
    this.gamesPerVenue();
  }

  getVenues(): void {
    this.dataService.getGames().subscribe(temp => {
      var tempArr = [];

      temp.forEach(element => {
        if (element.year == 2019){
        if(tempArr.indexOf(element.venue) == -1)
        tempArr.push(element.venue);
      }
    })
      this.venues = tempArr;
    })

  }

  gamesPerVenue(): void {

    this.venueCounter = 0;
    this.dataService.getGames().subscribe(temp => {

      temp.forEach(element => {
        if (element.year == 2019) {
          if (element.venue == this.selectedVenue) {
            this.venueCounter++;}
          }
        })
      })
    }


}
