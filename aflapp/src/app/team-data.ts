export class TeamData {

    constructor(
        public id: number, 
        public name: string, 
        public logo: string,
        public abbrev: string) {}
}
