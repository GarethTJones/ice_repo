import { Component, OnInit } from '@angular/core';
import {Game} from '../game';
import {DataserviceService} from '../dataservice.service';

@Component({
  selector: 'app-game-data',
  templateUrl: './game-data.component.html',
  styleUrls: ['./game-data.component.css']
})
export class GameDataComponent implements OnInit {
  
  games:Game[]; //Store the Game objects
  teams: import("../team-data").TeamData[];

  constructor(private dataService:DataserviceService) { }

  ngOnInit() {
    this.getAFLGames(); 
    this.getAFLTeams();
  }

  //Collects all AFL Games into an array
  getAFLGames(): void{
    this.dataService.getGames().subscribe(temp => {this.games = temp;})
  }

  //Collects all AFL Teams into an array
  getAFLTeams(): void{
    this.dataService.getTeams().subscribe(temp => this.teams = temp)
  }
}
