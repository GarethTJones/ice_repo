import { Component, OnInit } from '@angular/core';
import { TeamData } from '../team-data';
import { DataserviceService } from '../dataservice.service';
import { Game } from '../game';

@Component({
  selector: 'app-team-data',
  templateUrl: './team-data.component.html',
  styleUrls: ['./team-data.component.css']
})
export class TeamDataComponent implements OnInit {

  teams: TeamData[]; //Store the teamData objects
  games: Game[];
  nextGames: Game[];
  headToHead: Game[];

  selectedTeam: TeamData;
  selectedTeam2: TeamData;

  seasonGoals = 0;
  seasonBehinds = 0;
  seasonGoalsAgainst = 0;
  seasonBehindsAgainst = 0;
  avgGoals = 0;
  avgBehinds = 0;
  numGames = 0;
  isShow = false;
  isDisplay = true;

  ladderPos = 0;

  constructor(private dataService: DataserviceService) { }

  ngOnInit() {
    this.getAFLTeams();

  }

  //Collects all AFL Teams into an array
  getAFLTeams(): void {
    this.dataService.getTeams().subscribe(temp => { this.teams = temp; })
  }

  //Act as an event trigger. Instantiates a variable for the selected team.
  //Runs methods for collecting results for selected team, upcoming games and team's averages.
  //Hides the teams grid.
  onSelect(team: TeamData): void {
      this.selectedTeam = team;
      this.isShow = !this.isShow;
      this.getTeamResults();
      this.getUpcomingGames();
      this.getAverages();
      this.getAFLStanding();
  }

  H2H(team: TeamData): void {
    this.isDisplay = false;
    this.selectedTeam2 = team;
    this.getHeadToHead();
    

  }

  //Shows the teams grid again. Removes the selection for selected team.
  offSelect() {
    this.isShow = false;
    this.isDisplay = true;
    this.selectedTeam = null;
    this.selectedTeam2 = null;
  }

  //Colelcts an array of a team's results for the season so far
  getTeamResults(): void {
    this.dataService.getGames().subscribe(temp => {
      var tempArr = [];

      temp.forEach(element => {
        if (element.year == 2019 && element.round <= 20) {
          if (element.hteam == this.selectedTeam.name ||
            element.ateam == this.selectedTeam.name) tempArr.push(element);
        };
      });
      this.games = tempArr;
    })
  }

  //For the below, if we were not set to look through games that have already passed,
  //we would use "if(element.year == 2019 && element.complete == 0..." but we have
  //Set it out this way as per the brief.
  getUpcomingGames(): void {
    this.dataService.getGames().subscribe(temp => {
      var tempArr = [];

      temp.forEach(element => {
        if (element.year == 2019 && element.round >= 20) {
          if (element.hteam == this.selectedTeam.name ||
            element.ateam == this.selectedTeam.name) tempArr.push(element);
        };
      });
      this.nextGames = tempArr;
    })
  }

  //Averages the team's goals and behinds for the season.
  getAverages(): void {
    this.dataService.getGames().subscribe(temp => {
      var counter = 0;
      var tempGoals = 0;
      var tempBehinds = 0;
      var tempGoalsAgainst = 0;
      var tempBehindsAgainst = 0;

      temp.forEach(element => {
        if (element.year == 2019 && element.round <= 20) {
          if (element.hteam == this.selectedTeam.name) {
            counter++;
            tempGoals += element.hgoals;
            tempBehinds += element.hbehinds;
            tempGoalsAgainst += element.agoals;
            tempBehindsAgainst += element.abehinds;
          } else if (element.ateam == this.selectedTeam.name) {
            counter++;
            tempGoals += element.agoals;
            tempBehinds += element.abehinds;
            tempGoalsAgainst += element.hgoals;
            tempBehindsAgainst += element.hbehinds;
          }
          this.seasonGoals = tempGoals;
          this.seasonBehinds = tempBehinds;
          this.seasonGoalsAgainst = tempGoals;
          this.seasonBehindsAgainst = tempBehinds;
          this.avgGoals = Math.round(tempGoals / counter);
          this.avgBehinds = Math.round(tempBehinds / counter);
          this.numGames = counter;

        };
      });
    })
  }

  getHeadToHead() : void {
    this.dataService.getGames().subscribe(temp => {
      var tempArr = [];

      temp.forEach(element => {
        if (element.year == 2019 && element.round <= 20) {
          if (element.hteam == this.selectedTeam.name ||
            element.ateam == this.selectedTeam.name) {
              if (element.hteam == this.selectedTeam2.name ||
                element.ateam == this.selectedTeam2.name)
              tempArr.push(element);}
        };
      });
      this.headToHead = tempArr;
    })
  }

  getAFLStanding() : void{
    this.dataService.getStanding().subscribe(temp => {
      var tempLadderPos = 0;
      temp.forEach(element => {
        if (element.name == this.selectedTeam.name){
          tempLadderPos = element.rank;
        }
      });

      this.ladderPos = tempLadderPos;
    })
  }

}
