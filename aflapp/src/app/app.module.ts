import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {HttpClientModule} from '@angular/common/http';

import {AppRoutingModule} from './app-routing.module';

import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { TeamDataComponent } from './team-data/team-data.component';
import { DataserviceService } from './dataservice.service';
import { GameDataComponent } from './game-data/game-data.component';
import { LadderComponent } from './ladder/ladder.component';
import { TipDataComponent } from './tip-data/tip-data.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UpcomingGamesComponent } from './upcoming-games/upcoming-games.component';

@NgModule({
  declarations: [
    AppComponent,
    TeamDataComponent,
    GameDataComponent,
    LadderComponent,
    TipDataComponent,
    DashboardComponent,
    UpcomingGamesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [DataserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
