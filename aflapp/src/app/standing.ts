export class Standing {
    
    constructor (
        public goals_against: number,
        public id: number, 
        public against: number,
        public behinds_against: number,
        public played: number,
        public behinds_for: number,
        public goals_for: number,
        public wins: number,
        public rank: number, 
        public losses: number,
        public draws: number,
        public pts: number,
        public percentage: number,
        public name: string,
        public ptsfor: number) {}
    
}
