import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TeamDataComponent } from './team-data/team-data.component';
import { GameDataComponent } from './game-data/game-data.component';
import { LadderComponent } from './ladder/ladder.component';
import { TipDataComponent } from './tip-data/tip-data.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UpcomingGamesComponent } from './upcoming-games/upcoming-games.component';

const routes: Routes = [
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'Teams', component: TeamDataComponent},
  {path: 'Games', component: GameDataComponent},
  {path: 'Ladder', component:LadderComponent },
  {path: 'Tips', component:TipDataComponent },
  {path: 'UpcomingGames', component:UpcomingGamesComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }