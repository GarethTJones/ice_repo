import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { TeamData } from './team-data';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Game } from './game';
import { Standing } from './standing';
import { Tip } from './tip';

@Injectable({
  providedIn: 'root'
})
export class DataserviceService {
  

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getTeams();
    this.getGames();
    this.getStanding();
    this.getTips();
  }
  getTeams() : Observable<TeamData[]> {

    return this.http.get('https://api.squiggle.com.au/?q=teams').pipe(
      map((data: any) => data.teams.map((item: any) => new TeamData(
        item.id,
        item.name,
        item.logo,
        item.abbrev
      )))
    );
  }
  getGames() : Observable<Game[]> {
    return this.http.get('https://api.squiggle.com.au/?q=games').pipe(
      map((data: any) => data.games.map((item:any) => new Game(
        item.complete,
        item.is_grand_final,
        item.tz,
        item.hbehinds,
        item.ateam,
        item.winnerteamid,
        item.hgoals,
        item.updated,
        item.round,
        item.is_final,
        item.hscore,
        item.abehinds,
        item.winner,
        item.ascore,
        item.hteam,
        item.ateamid,
        item.venue,
        item.hteamid,
        item.agoals,
        item.year,
        item.date,
        item.id
      )))
    );
  }
  getStanding() : Observable<Standing[]> {
    return this.http.get('https://api.squiggle.com.au/?q=standings;round=20;year=2019').pipe(
      map((data: any) => data.standings.map((item:any) => new Standing(
        item.goals_against,
        item.id,
        item.against,
        item.behinds_against,
        item.played,
        item.behinds_for,
        item.goals_for,
        item.wins,
        item.rank ,
        item.losses,
        item.draws,
        item.pts,
        item.percentage,
        item.name,
        item.for
      )))
    );
  }
  getTips() : Observable<Tip[]> {
    return this.http.get('https://api.squiggle.com.au/?q=tips').pipe(
      map((data: any) => data.tips.map((item:any) => new Tip(
        item.correct,
        item.hteamid,
        item.updated,
        item.hconfidence,
        item.margin,
        item.hteam,
        item.ateam,
        item.year,
        item.tipteamid,
        item.bits,
        item.err,
        item.gameid,
        item.sourceid,
        item.round,
        item.ateamid,
        item.venue,
        item.date,
        item.tip,
        item.source,
        item.pconfidence
      )))
    );
  }
}
