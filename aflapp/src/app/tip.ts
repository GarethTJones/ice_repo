export class Tip {
    
    constructor(
        public correct: number,
        public hteamid: number,
        public updated: string,
        public hconfidence: string,
        public margin: string,
        public hteam: string,
        public ateam: string,
        public year: number,
        public tipteamid: number,
        public bits: string,
        public err: string,
        public gameid: number,
        public sourceid: number,
        public round: number,
        public ateamid: number,
        public venue: string,
        public date: string,
        public tip: string,
        public source: string,
        public confidence: string, 
    ) {}
}
