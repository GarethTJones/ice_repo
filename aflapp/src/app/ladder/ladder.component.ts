import { Component, OnInit } from '@angular/core';
import { Standing } from '../standing';
import { DataserviceService } from '../dataservice.service';
import { TeamData } from '../team-data';

@Component({
  selector: 'app-ladder',
  templateUrl: './ladder.component.html',
  styleUrls: ['./ladder.component.css']
})
export class LadderComponent implements OnInit {

  standings: Standing[]; //Store the Standing objects
  teams: TeamData[]; //Store the teamData objects

  selectedTeam: TeamData;
  selectedStanding: Standing;

  constructor(private dataService: DataserviceService) { }

  ngOnInit() {
    this.getAFLLadder();
  }

  //Collects AFL Team's standings into an array
  getAFLLadder(): void {
    this.dataService.getStanding().subscribe(temp => { this.standings = temp; })
  }
}