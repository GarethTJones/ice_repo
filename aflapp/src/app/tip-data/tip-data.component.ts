import { Component, OnInit } from '@angular/core';
import { DataserviceService } from '../dataservice.service';
import { Tip } from '../tip';
import { TeamData } from '../team-data';


@Component({
  selector: 'app-tip-data',
  templateUrl: './tip-data.component.html',
  styleUrls: ['./tip-data.component.css'],
})
export class TipDataComponent implements OnInit {

  tips: Tip[];
  selectedTeam: TeamData;
  teams: TeamData[];
  isShow = true;

  constructor(private dataService: DataserviceService) { }

  ngOnInit(): void {
    this.getAFLTips();
    this.getAFLTeams();

  }

  getAFLTips(): void {
    this.dataService.getTips().subscribe(temp => { this.tips = temp; })
  }

  getAFLTeams(): void {
    this.dataService.getTeams().subscribe(temp => { this.teams = temp; })
  }



  onSelect(team: TeamData): void {
    this.selectedTeam = team;
    this.getTeamTips(this.selectedTeam);
  }

  getTeamTips(team: TeamData): void {
    this.dataService.getTips().subscribe(temp => {
      var tempArr = [];
      this.isShow = true;

      temp.forEach(element => {
        if (element.year == 2019 && element.round == 20) {
          if (element.tip == this.selectedTeam.name) tempArr.push(element);
        };
      });
      if (tempArr.length == 0) {
        this.isShow = false;
      }
      this.tips = tempArr;
      this.tips.sort(this.compareFunc);
    })
  }

  compareFunc(a, b) {
    const gameARound = a.round;
    const gameBRound = b.round;

    let compare = 0;

    if (gameARound < gameBRound) {
      compare = -1;
    } else if (gameARound > gameBRound) {
      compare = 1;
    }
    return compare;
  }

}

